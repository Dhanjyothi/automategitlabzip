package com.hcl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.hcl.main.Main;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class CreateEmployee {
    Main main;
    private int id;
    private String name;
    private int age;
    List<String> empList;

       @Given("^User wants to store the  data$")
    public void user_wants_to_store_the_data()throws Throwable {
           main = new Main();
           empList=new ArrayList<String>();
    }

    @When("^User enters the id as (\\d+) and name as \"([^\"]*)\" and age as (\\d+)$")
    public void user_enters_the_id_as_and_name_as_and_age(Integer id,String name,Integer age) throws Throwable {
        this.id = id;
        this.name = name;
        this.age = age;
        
    }
   /* @And("^Fetch data from database for creation$")
    public void fetch_data_from_database_for_creation(DataTable dt)
        {
        List<String> list = dt.asList(String.class);
       empList.add(list.get(0));
        empList.add(list.get(1));
        empList.add(list.get(2));
        }
    */
    //Data saved successfully and it returns as 1 and the count as 3
    @Then("^Data saved successfully and it returns as (\\d+) and the count as (\\d+)$")
    public void data_saved_successfully_and_it_returns_as_and_the_count_as(int result,int count) throws Throwable {
        List list_before=main.fetch();
       
        Assert.assertEquals(result,main.create(id,name,age));
        List list_after=main.fetch();
       
        Assert.assertEquals(count,list_after.stream().count()-list_before.stream().count());
      
        
    }
}
