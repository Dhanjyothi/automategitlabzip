package com.hcl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Employee;
import com.hcl.util.ConnectionData;

public class EmployeeDao {
    ConnectionData connection = new ConnectionData();
    Employee emp = new Employee();

   /* public int create(Integer id, String name, Integer age) throws SQLException, ClassNotFoundException {
      
    }*/

    public List<Employee> fetch() throws SQLException, ClassNotFoundException {
        Connection con = connection.getConnection();
        List<Employee> list=new ArrayList();
        PreparedStatement stmt = con.prepareStatement("select * from emp");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            emp.setId(rs.getInt(1));
            emp.setName(rs.getString(2));
            emp.setAge(rs.getInt(3));
            list.add(emp);
        }
        return list;
    }

}
